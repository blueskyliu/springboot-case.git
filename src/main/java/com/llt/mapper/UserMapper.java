package com.llt.mapper;

import com.llt.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

//这个注解表示这是mybatis mapper 类
@Mapper
@Repository//存储层
public interface UserMapper {
    List<User> queryUserList();
    User queryUserById(Integer id);
    User queryUserByName(String name);
    int addUser(User user);
    int updateUser(User user);
    int deleteUser(Integer id);

}
