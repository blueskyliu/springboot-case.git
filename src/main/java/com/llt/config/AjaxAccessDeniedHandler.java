//package com.llt.config;
//
//import com.llt.pojo.AjaxResponseBody;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.web.access.AccessDeniedHandler;
//import org.springframework.stereotype.Controller;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//@Configuration
//public class AjaxAccessDeniedHandler implements AccessDeniedHandler {
//    @Override
//    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
//        AjaxResponseBody responseBody = new AjaxResponseBody();
//
//        responseBody.setStatus("300");
//        responseBody.setMsg("Need Authorities!");
//
//        httpServletResponse.getWriter().write(responseBody.toString());
//    }
//}
