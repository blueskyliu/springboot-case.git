//package com.llt.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
////@EnableWebSecurity
//public class SecurtyConfig extends WebSecurityConfigurerAdapter {
//    @Autowired
//    AjaxAuthenticationEntryPoint authenticationEntryPoint;  //  未登陆时返回 JSON 格式的数据给前端（否则为 html）
//
//    @Autowired
//    AjaxAuthenticationSuccessHandler authenticationSuccessHandler;  // 登录成功返回的 JSON 格式数据给前端（否则为 html）
//
//    @Autowired
//    AjaxAuthenticationFailureHandler authenticationFailureHandler;  //  登录失败返回的 JSON 格式数据给前端（否则为 html）
//    //授权规则
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
////        所有页面可以访问 功能页面有权限才可以访问
////        antMatchers 访问路径  permitAll 所有人都可以访问
//        //hasRole 访问规则/权限·
//        http.authorizeRequests()
//         .antMatchers("/getuserlist").permitAll()
//        .antMatchers("/adduser").hasRole("admin1")
//        .and()
//          .httpBasic()
//           .authenticationEntryPoint(authenticationEntryPoint)
//
//        ;
//
//        //没权限进入登录页面 开启登录
//        http.formLogin()
//                .loginProcessingUrl("/adminlogin")
//                .successHandler(authenticationSuccessHandler) // 登录成功
//                .failureHandler(authenticationFailureHandler) // 登录失败
//        ;
//        //注销
//        http.logout()
//        .logoutUrl("/logout");
//    }
//    //认证规则
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        //withUser 用户 password密码 roles 对应的规则
//        //正常从数据库读取
//        auth.inMemoryAuthentication()
//                .passwordEncoder(new BCryptPasswordEncoder())
//         .withUser("admin")
//                .password(new BCryptPasswordEncoder().encode("123456")).roles("admin1")
//        .and()
//            .withUser("lantian")
//            .password(new BCryptPasswordEncoder().encode("123456")).roles("admin2")
//
//        ;
//
//    }
//}
