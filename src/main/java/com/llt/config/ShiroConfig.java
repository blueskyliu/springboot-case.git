package com.llt.config;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    //ShiroFilterFactoryBean 传到前端
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean
    (@Qualifier("getDefaultWebSecurityManager") DefaultWebSecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        //设置安全管理器
        shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager);
        //添加内置过滤器
//        anon 无需认证就可以访问
       // authc 必须认证了才能让问
        //user 必须拥有记住我功能才能用
        //perms 拥有对某个资源的权限才能访问
        //role 拥有某个角色权限才能访问
        Map<String,String> filter = new LinkedHashMap<>();
        filter.put("/getuserlist","anon");
        filter.put("/adduser","authc");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filter);
//        shiroFilterFactoryBean.setLoginUrl("/tologin");
        return shiroFilterFactoryBean;
    }
    //Dafaul twebSecurityManager 接管对象
    @Bean(name = "getDefaultWebSecurityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
       //关联UserRealm
        defaultWebSecurityManager.setRealm(userRealm);
        return defaultWebSecurityManager;
    }
    //创建realm 对象
    @Bean(name = "userRealm")
    public UserRealm userRealm(){
        return  new UserRealm();
    }
}
