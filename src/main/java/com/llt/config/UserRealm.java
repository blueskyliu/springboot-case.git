package com.llt.config;

import com.llt.pojo.User;
import com.llt.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

public class UserRealm extends AuthorizingRealm {
    @Autowired
    UserService userService;
    //授权·1
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("执行了 授权 doGetAuthorizationInfo");
        return null;
    }
    //认证·2
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("执行了 认证 doGetAuthenticationInfo");
        String name = "root";
        String password = "123456";
        UsernamePasswordToken userTokens = (UsernamePasswordToken)authenticationToken;
        if (!userTokens.getUsername().equals(name)){
            return null;
        }
        User user = userService.queryUserName(userTokens.getUsername());
        if(user==null){
//            没这个人
            return null;
        }
        return new SimpleAuthenticationInfo("",password,"");
    }
}
