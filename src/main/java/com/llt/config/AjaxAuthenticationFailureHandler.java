//package com.llt.config;
//
//import com.llt.pojo.AjaxResponseBody;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.AuthenticationFailureHandler;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//@Configuration
//public class AjaxAuthenticationFailureHandler implements AuthenticationFailureHandler {
//    @Override
//    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
//        AjaxResponseBody responseBody = new AjaxResponseBody();
//        responseBody.setStatus("400");
//        responseBody.setMsg("Login Failure!");
//        httpServletResponse.getWriter().write(responseBody.toString());
//    }
//}
