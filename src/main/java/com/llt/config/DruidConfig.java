package com.llt.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
public class DruidConfig {
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druidDataSource(){
        return  new DruidDataSource();
    }
    //后台监控 web.xml bean 相当于 之前的spring的xml配置载入容器
    //引入spring boot内置了servlet容器所以没有web.xml
    //替代方法 ServletRegistrationBean
    @Bean
    public ServletRegistrationBean a(){
        ServletRegistrationBean<StatViewServlet> statViewServletServletRegistrationBean =
                new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
    //后台需要有人登录，账户密码配置
        HashMap<String, String> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("loginUsername","admin");
        objectObjectHashMap.put("loginPassword","123456");
        //允许谁可以访问
        objectObjectHashMap.put("allow","");
        //禁止谁能访问
        objectObjectHashMap.put("狂胜","192.168.2.6");
        //设置初始化参数
        statViewServletServletRegistrationBean.setInitParameters(objectObjectHashMap);
    return statViewServletServletRegistrationBean;
    }
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean<Filter> filterFilterRegistrationBean
                = new FilterRegistrationBean<>();
        filterFilterRegistrationBean.setFilter(new WebStatFilter());
        //可以过滤那些请求
        HashMap<String, String> objectObjectHashMap = new HashMap<>();
        //这些不进行统计
        objectObjectHashMap.put("exclusions","*.js,*.css,/druid/*");
        filterFilterRegistrationBean.setInitParameters(objectObjectHashMap);
        return filterFilterRegistrationBean;
    }
}
