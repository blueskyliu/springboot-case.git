//package com.llt.config;
//
//import com.llt.pojo.AjaxResponseBody;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//@Configuration
//public class AjaxAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
//
//    @Override
//    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
//        AjaxResponseBody responseBody = new AjaxResponseBody();
//        responseBody.setStatus("200");
//        responseBody.setMsg("Login Success!");
//        httpServletResponse.getWriter().write(responseBody.toString());
//    }
//}
