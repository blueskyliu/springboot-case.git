//package com.llt.config;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.llt.pojo.AjaxResponseBody;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.InsufficientAuthenticationException;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.AuthenticationEntryPoint;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//
////@EnableWebSecurity
//public class SecurtyJSONConfig extends WebSecurityConfigurerAdapter {
//    @Autowired
//    AjaxAuthenticationEntryPoint authenticationEntryPoint; //未登陆时返回 JSON 格式的数据给前端（否则为 html）
//    //授权规则
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/getuserlist").permitAll()
//                .antMatchers("/adduser").hasRole("admin1")
//                .and()
//                .httpBasic()
//                .authenticationEntryPoint(authenticationEntryPoint);
//    }
//    //认证规则
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        //withUser 用户 password密码 roles 对应的规则
//        //正常从数据库读取
//        auth.inMemoryAuthentication()
//                .passwordEncoder(new BCryptPasswordEncoder())
//         .withUser("admin")
//                .password(new BCryptPasswordEncoder().encode("123456")).roles("admin1")
//        .and()
//            .withUser("lantian")
//            .password(new BCryptPasswordEncoder().encode("123456")).roles("admin2")
//
//        ;
//
//    }
//}
