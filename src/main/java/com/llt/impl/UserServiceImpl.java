package com.llt.impl;

import com.llt.mapper.UserMapper;
import com.llt.pojo.User;
import com.llt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl  implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getAll() {
        return userMapper.queryUserList();
    }

    @Override
    public User getUserone(Integer id) {
        return userMapper.queryUserById(id);
    }

    @Override
    public int addUser(User user) {
        int i = userMapper.addUser(user);
        return i;
    }

    @Override
    public User queryUserName(String name) {
        return userMapper.queryUserByName(name);
    }

    @Override
    public int updateUser(User user) {
        return 0;
    }

    @Override
    public int deleteUser(Integer id) {
        return 0;
    }
}
