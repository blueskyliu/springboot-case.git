package com.llt.pojo;

public class User {
    private Integer id;
    private String user;
    private String passd;

    public User() {

    }

    public User(Integer id, String user, String passd) {
        this.id = id;
        this.user = user;
        this.passd = passd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassd() {
        return passd;
    }

    public void setPassd(String passd) {
        this.passd = passd;
    }
}
