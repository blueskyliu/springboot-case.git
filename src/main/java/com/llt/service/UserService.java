package com.llt.service;

import com.llt.pojo.User;

import java.util.List;

public interface UserService {
    List<User> getAll();
    User getUserone(Integer id);
    int addUser(User user);
    User queryUserName(String name);
    int updateUser(User user);
    int deleteUser(Integer id);
}
