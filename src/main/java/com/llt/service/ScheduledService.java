package com.llt.service;

        import org.springframework.scheduling.annotation.Scheduled;
        import org.springframework.stereotype.Service;

@Service
public class ScheduledService {
    //在特定时间执行代码
    //秒 分 时 日 月 周几
    @Scheduled(cron = "0/2 * * * * ?")
    public void hello(){
        System.out.println("hello 你被执行了");
    }
}
