package com.llt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

//@RestController
public class JdbcController {
    @Autowired
    JdbcTemplate jdbcTemplate;
//    查询数据库所有信息
    @GetMapping("/userlist")
    public List<Map<String,Object>> userList(){
        String sql = "select * from users";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        return maps;
    }
    @GetMapping("/adduser")
    public String addUser(){
        String sql = "insert into users(user,passd) values('明','123la')";

        jdbcTemplate.update(sql);
        return "update ok";
    }
    @GetMapping("/updateuser")
    @ResponseBody
    public String updateUser(@RequestParam String name,@RequestParam String passd,@RequestParam Integer id){
        String sql = "update mybatis.users set user=?,passd=? where id =?";
        Object[] objects = new Object[3];
        objects[0] = name;
        objects[1] = passd;
        objects[2] = id;
        jdbcTemplate.update(sql,objects);
        return "update ok";
    }
    @GetMapping("/deleteuser")
    public String deleteUser(@RequestParam Integer id){
        String sql = "delete from mybatis.users where id =?";
        jdbcTemplate.update(sql,id);
        return "delete ok";
    }
}
